const restify = require('restify');
const errs = require('restify-errors')

const server = restify.createServer({
  name: 'spa_nodejs',
  version: '1.0.0'
});

const knex = require('knex')({
    client: 'mysql',
    connection: {
      host : '127.0.0.1',
      user : 'root',
      password : '',
      database : 'db_node_spa'
    }
  });

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());


server.listen(8080, function () {
  console.log('%s listening at %s', server.name, server.url);
});


// Select
server.get('/', function (req, res, next) {

    knex('rest').then((dados) => {

        res.send(dados);

    }, next);

    return next();

  });

// Insert
  server.post('/create', function (req, res, next) {

    knex('rest')
    .insert(req.body)
    .then( (dados) => {

      res.send(dados);

    }, next );

  });


  // Selecionar pelo o ID
  server.get('/show/:id', function (req, res, next) {
    // pegando o valor dinamico do id parametro
    const { id } = req.params;
    knex('rest')

    .where('id', id)
    .first()
    .then((dados) => {

      if (!dados) return res.send(new errs.BadRequestError('Nada foi encontrado'))

        // Se tiver dados, ele responde 
        res.send(dados);

    }, next);

  });


// Update
server.put('/update/:id', function (req, res, next) {
  // pegando o valor dinamico do id parametro
  const { id } = req.params;
  knex('rest')
    .where('id', id)
    .update(req.body) // esse cara esta no postman, na aba BODY, pois o req é o campo de URL
    .then((dados) => {

      if (!dados) return res.send(new errs.BadRequestError('Nada foi encontrado'))

        // Se tiver dados, ele responde 
        res.send('Dados Atualizados!');

    }, next);

});

// Delete
server.del('/delete/:id', function (req, res, next) {

  const { id } = req.params;
  knex('rest')
    .where('id', id)
    .del(req.body)
    .then( (dados) => {

      if (!dados) return res.send(new errs.BadRequestError('Nada foi encontrado!'))
        res.send(`ID:${id} deletado!`);
    }, next);
});